#ifndef EDITAUTHORDIALOG_H
#define EDITAUTHORDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include "author.hpp"

namespace Ui {
class EditAuthorDialog;
}

class EditAuthorDialog : public QDialog
{
    Q_OBJECT
signals:
    void sendUpdatedAuthor(Author);
public:
    explicit EditAuthorDialog(QWidget *parent = 0);
    ~EditAuthorDialog();
private slots:

    void on_Cansel_Ok_accepted();

    void on_Cansel_Ok_rejected();

    void getAuthorToEdit(QListWidgetItem* item);

private:
    Ui::EditAuthorDialog *ui;
};

#endif // EDITAUTHORDIALOG_H
