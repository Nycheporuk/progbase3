#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidgetItem>
#include "book.hpp"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    ~EditDialog();
signals:
    void sendUpdatedBook(Book*);

private slots:

    void on_Cansel_Ok_accepted();

    void on_Cansel_Ok_rejected();

    void getBookToEdit(QListWidgetItem* item);

    void on_ImageButton_clicked();

private:
    Ui::EditDialog *ui;
    QString path;
};

#endif // EDITDIALOG_H
