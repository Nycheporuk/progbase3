#-------------------------------------------------
#
# Project created by QtCreator 2019-06-06T00:31:30
#
#-------------------------------------------------

QT       += gui sql core

TARGET = StaticLib
TEMPLATE = lib
CONFIG += staticlib c++14 console
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


SOURCES += \
    staticlib.cpp\
    book.cpp \
    sqlite_storage.cpp \
    csv_storage.cpp \
    generatorId.cpp\
    csv.cpp

HEADERS += \
    staticlib.h\
    storage.hpp\
    book.hpp\
    author.hpp\
    sqlite_storage.h \
    user.h \
    csv_storage.hpp \
    csv.hpp \
    optional.hpp\
    generatorId.hpp





unix {
    target.path = /usr/lib
    INSTALLS += target
}
