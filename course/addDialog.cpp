#include "addDialog.h"
#include "ui_addDialog.h"
#include "book.hpp"
#include "mainWindow.h"
AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_Cansel_Ok_accepted()
{
    Book * book = new Book();
    book->name_ = ui->nameLine->text().toStdString();
    book->author_ = ui->authorLine->text().toStdString();
    book->chapters_ = ui->chaptersSpinBox->value();
    book->image_path =  path.toStdString();
    emit sendBook(book);
    AddDialog::close();

}

void AddDialog::on_Cansel_Ok_rejected()
{
    AddDialog::close();
}



void AddDialog::on_ImageButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Dialog Caption",
                "",
                "IMG (*.jpg);;All Files (*)");


    path = fileName;
    qDebug() << fileName;
}
