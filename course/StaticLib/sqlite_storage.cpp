#include "sqlite_storage.h"

#include <QDebug>
#include <QtSql>

SqliteStorage::SqliteStorage(const string &dir_name) : dir_name_(dir_name)
{
    if (!db_.isOpen())
        db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
        qDebug() << "Error. Can't connect";

        return false;
    }
    qDebug() << "DB opened ";

    return true;
}



bool SqliteStorage::close()
{
    db_.close();
    qDebug() << "DB closed ";

    return true;
}


Book getBookFromQuery(const QSqlQuery &query)
{
    Book book;
    book.id_ = query.value("id").toInt();
    book.name_ = query.value("name").toString().toStdString();
    book.author_ = query.value("author").toString().toStdString();
    book.chapters_ = query.value("chapters").toInt();
    book.image_path = query.value("image_path").toString().toStdString();
    return book;
}

vector<Book> SqliteStorage::getAllBooks(void)
{

    vector<Book> books;
    QSqlQuery query("SELECT * FROM books");
    while (query.next())
    {
        books.push_back(getBookFromQuery(query));
    }
    return books;
}
vector<Book> SqliteStorage::getAllUserBooks(int user_id)
{

    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE user_id = :user_id ");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "getAllUserBooks error:" << query.lastError();
        return books;
    }
    while (query.next())
    {
        books.push_back(getBookFromQuery(query));
    }
    return books;
}
optional<Book> SqliteStorage::getBookById(int book_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", book_id);
    if (!query.exec())
    {
        qDebug() << "get Book error:" << query.lastError();
        return nullopt;
    }
    if (query.next())
    {
        return getBookFromQuery(query);
    }
    return nullopt;
}

bool SqliteStorage::updateBook(const Book &book)
{
    QSqlQuery query;
    query.prepare("UPDATE books SET name = :name, author = :author, chapters = :chapters, image_path = :image_path WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(book.name_));
    query.bindValue(":author", QString::fromStdString(book.author_));
    int chapters = book.chapters_;
    query.bindValue(":chapters", chapters);
    int id = book.id_;
    query.bindValue(":id", id);
    query.bindValue(":image_path", QString::fromStdString(book.image_path));

    if (!query.exec())
    {
        qDebug() << "updateBook error:" << query.lastError();
        return false;
    }
    if ((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeBook(int id)
{

    QSqlQuery query;
    query.prepare("DELETE FROM books WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec())
    {
        qDebug() << "deleteBook error:" << query.lastError();
        return false;
    }
    if ((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertBook(const Book &book)
{

    QSqlQuery query;
    query.prepare("INSERT INTO books (name, author, chapters) VALUES (:name, :author, :chapters)");
    query.bindValue(":name", QString::fromStdString(book.name_));
    query.bindValue(":author", QString::fromStdString(book.author_));
    int chapters = book.chapters_;
    query.bindValue(":chapters", chapters);

    if (!query.exec())
    {
        qDebug() << "cant insert book" << query.lastError();
        return 0;
    }

    QVariant var = query.lastInsertId();

    return var.toInt();
}

int SqliteStorage::insertBook(const Book &book, int user_id)
{

    QSqlQuery query;
    query.prepare("INSERT INTO books (name, author, chapters, user_id, image_path) VALUES (:name, :author, :chapters, :user_id, :image_path)");
    query.bindValue(":name", QString::fromStdString(book.name_));
    query.bindValue(":author", QString::fromStdString(book.author_));
    int chapters = book.chapters_;
    query.bindValue(":chapters", chapters);
    query.bindValue(":user_id", user_id);
    query.bindValue(":image_path", QString::fromStdString(book.image_path));

    if (!query.exec())
    {
        qDebug() << "cant insert book" << query.lastError();
        return 0;
    }

    QVariant var = query.lastInsertId();

    return var.toInt();
}


Author getAuthorFromQuery(const QSqlQuery & query)
{
    Author author;
    author.id_ = query.value("id").toInt();
    author.name_ = query.value("name").toString().toStdString();
    author.country_ = query.value("country").toString().toStdString();
    author.year_ = query.value("year").toInt();
    return author;
}


optional<Author> SqliteStorage::getAuthorById(int author_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM authors WHERE id = :id");
    query.bindValue(":id", author_id);
    if (!query.exec())
    {
        qDebug() << "get author error:" << query.lastError();
        return nullopt;
    }
    if (query.next())
    {
        return getAuthorFromQuery(query);
    }
    return nullopt;
}

int SqliteStorage::insertAuthor(const Author &author, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO authors (name, country, year, user_id) VALUES (:name, :country, :year, :user_id)");
    query.bindValue(":name", QString::fromStdString(author.name_));
    query.bindValue(":country", QString::fromStdString(author.country_));
    int year = author.year_;
    query.bindValue(":year", year);
    query.bindValue(":user_id", user_id);
    if(!query.exec())
    {
        qDebug() << "cant insert author" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

bool SqliteStorage::removeAuthor(int id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM authors WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "deleteAuthor error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::updateAuthor(const Author &author)
{
    QSqlQuery query;
    query.prepare("UPDATE authors SET (name = :name, country = :country, year = :year) WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(author.name_));
    query.bindValue(":country", QString::fromStdString(author.country_));
    int year = author.year_;
    query.bindValue(":year", year);
    int id = author.id_;
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "updateAuthor error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

vector<Author> SqliteStorage::getAllUserAuthors(int user_id)
{
    QSqlQuery query;
    vector<Author> authors;
    query.prepare("SELECT * FROM authors WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << "getAllUserAuthors error:" << query.lastError();
       return authors;
    }
    while (query.next())
    {
        authors.push_back(getAuthorFromQuery(query));
    }
    return authors;
}

QString hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

// users
optional<User> SqliteStorage::getUserAuth(string & username, string & password)
{

    QSqlQuery query;
    optional<User> opt;
    query.prepare("SELECT * FROM users WHERE (username = :username AND password_hash = :password_hash)");
    query.bindValue(":username", QString::fromStdString(username));
    QString p = hashPassword(QString::fromStdString(password));
    query.bindValue(":password_hash", p);
    if (!query.exec())
    {
        qDebug() << "Errorin query exec" << query.lastError();
        return nullopt;
    }
    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = username;
        user.password_hash = hashPassword(QString::fromStdString(password)).toStdString();
        opt = user;
        return opt;
    }
    return nullopt;
}

// links
vector<Author> SqliteStorage::getAllBookAuthors(int book_id, int user_id)
{
    vector<Author> authors;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE book_id = :book_id AND user_id = :user_id;");
    query.bindValue(":user_id", user_id);
    query.bindValue(":book_id", book_id);
    if (!query.exec())
    {
       qDebug() << "getAllBookAuthors error:" << query.lastError();
       return authors;
    }
    while (query.next())
    {
        qDebug() << "author_id id: " << query.value("author_id").toInt();
        optional<Author> opt = getAuthorById(query.value("author_id").toInt());
        if (!opt)
        {
            qDebug() << "Error getting opt";
            continue;
        }
        authors.push_back(opt.value());
    }
    return authors;
}

bool isPresent(int book_id, int author_id, int user_id)
{
    QSqlQuery query_test;
    query_test.prepare("SELECT book_id, author_id, user_id FROM links WHERE book_id = :book_id AND author_id = :author_id AND user_id = :user_id");
    query_test.bindValue(":book_id", book_id);
    query_test.bindValue(":author_id", author_id);
    query_test.bindValue(":user_id", user_id);
    if (!query_test.exec())
    {
        return true;
    }
    if (query_test.next())
    {
        if (query_test.value("book_id").toInt() == book_id)
            return true;
    }
    return false;
}

bool SqliteStorage::insertBookAuthor(int book_id, int author_id, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (book_id, author_id, user_id) VALUES (:book_id, :author_id, :user_id)");
    query.bindValue(":book_id", book_id);
    query.bindValue(":author_id", author_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "insertBookAuthor error int sqlite_storage.cpp:" << query.lastError();
        return false;
    }
    return true;
}

bool SqliteStorage::removeBookAuthor(int book_id, int author_id, int user_id)
{
    if (!isPresent(book_id, author_id, user_id))
    {
        return false;
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE book_id = :book_id AND author_id = :author_id AND user_id = :user_id;");
    query.bindValue(":book_id", book_id);
    query.bindValue(":author_id", author_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeByBookId(int book_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE book_id = :book_id AND user_id = :user_id;");
    query.bindValue(":book_id", book_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeByAuthorId(int author_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE author_id = :author_id AND user_id = :user_id;");
    query.bindValue(":author_id", author_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}
bool SqliteStorage::addUser(QString & username, QString & password){
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username");
    query.bindValue(":username", (username));
    if (!query.exec())
    {
        qDebug() <<"exec error in add user";

        return false;
    }
    if(query.next()){
        qDebug() <<"User already exist";
        return false;
    }
    QSqlQuery query2;
    query2.prepare("INSERT INTO users (username, password_hash) VALUES (:username, :password_hash)");
    query2.bindValue(":username", (username));
    QString p = hashPassword(password);
    query2.bindValue(":password_hash", p);
    if (!query2.exec())
    {
        qDebug() << "Error query exec in addUser" << query.lastError();
        return false;
    }
    return true;
}
vector<Book> SqliteStorage::getPageOfBooks(int user_id, int current_page, int page_size){
    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE user_id = :user_id LIMIT :page_size OFFSET :skipped_items");
    query.bindValue(":user_id", user_id);
    query.bindValue(":page_size", page_size);
    int skipped_items = (current_page - 1) * page_size;
    query.bindValue(":skipped_items", QString::fromStdString( to_string(skipped_items)));

    if (!query.exec())
    {
        qDebug() << "getPageOfBooks error:" << query.lastError();
        return books;
    }
    while (query.next())
    {
        books.push_back(getBookFromQuery(query));
    }
    return books;
}
