#include "editDialog.h"
#include "ui_editDialog.h"
#include <QFileDialog>
#include <QDebug>
EditDialog::EditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{

    ui->setupUi(this);
}

EditDialog::~EditDialog()
{
    delete ui;
}



void EditDialog::on_Cansel_Ok_accepted()
{
    Book * book = new Book();
    book->name_ = ui->nameLine->text().toStdString();
    book->author_ = ui->authorLine->text().toStdString();
    book->chapters_ = ui->chaptersSpinBox->value();
    book->image_path = path.toStdString();
    emit sendUpdatedBook(book);
    EditDialog::close();
}


void EditDialog::on_Cansel_Ok_rejected()
{
    EditDialog::close();
}

void EditDialog::getBookToEdit(QListWidgetItem* item)
{
    QVariant variant = item->data(Qt::UserRole);
    Book book = variant.value<Book>();

    ui->nameLine->setText(QString::fromStdString(book.name_));
    ui->authorLine->setText(QString::fromStdString(book.author_));
    ui->chaptersSpinBox->setValue(book.chapters_);
    path = QString::fromStdString(book.image_path);
}


void EditDialog::on_ImageButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Dialog Caption",
                "",
                "IMG (*.jpg);;All Files (*)");

    if(!fileName.isEmpty()){
        path = fileName;

    }
    qDebug() << fileName;
}
