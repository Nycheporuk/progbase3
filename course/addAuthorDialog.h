#ifndef ADDAUTHORDIALOG_H
#define ADDAUTHORDIALOG_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QMessageBox>

#include "author.hpp"

namespace Ui {
class AddAuthorDialog;
}

class AddAuthorDialog : public QDialog
{
    Q_OBJECT
signals:
    void sendAuthor(Author*);

public:
    explicit AddAuthorDialog(QWidget *parent = 0);
    ~AddAuthorDialog();
private slots:
    void on_Cansel_Ok_accepted();

    void on_Cansel_Ok_rejected();

private:
    Ui::AddAuthorDialog *ui;
};

#endif // ADDAUTHORDIALOG_H
