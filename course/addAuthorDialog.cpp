#include "addAuthorDialog.h"
#include "ui_addAuthorDialog.h"

AddAuthorDialog::AddAuthorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddAuthorDialog)
{
    ui->setupUi(this);
}

AddAuthorDialog::~AddAuthorDialog()
{
    delete ui;
}
void AddAuthorDialog::on_Cansel_Ok_accepted()
{
    Author * author = new Author();
    author->name_ = ui->nameLine->text().toStdString();
    author->country_ = ui->countryLine->text().toStdString();
    author->year_ = ui->yearSpinBox->value();

    emit sendAuthor(author);
    AddAuthorDialog::close();

}

void AddAuthorDialog::on_Cansel_Ok_rejected()
{
    AddAuthorDialog::close();
}
