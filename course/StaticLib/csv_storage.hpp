#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "csv.hpp"
#include "storage.hpp"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
    const string dir_name_;

    vector<Book> books_;
    vector<Author> authors_;

    Book rowToBook(const CsvRow & row);
    CsvRow BookToRow(const Book & book);
    Author rowToAuthor(const CsvRow & rowe);
    CsvRow authorToRow(const Author & author);

    int getNewBookId(string dir_name);
    int getNewAuthorId(string dir_name);

    void clearIdFile();
    void deleteAll();

    int getSizeFromFile(string file_name);

public:
    CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

    bool load();
    bool save();

    // books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int id);
    bool updateBook(const Book & book);
    bool removeBook(int id);
    int insertBook(const Book & book);
    // authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int );
    bool updateAuthor(const Author &author);
    bool removeAuthor(int id);
    int insertAuthor(const Author &author);
    int insertBook(const Book & Book, int user_id);
    vector<Book> getAllUserBooks(int user_id); //new DONE

    int insertAuthor(const Author & author, int user_id);
    bool removeAuthor(int author_id, int user_id);
    vector<Author> getAllUserAuthors(int user_id);

    // users
    optional<User> getUserAuth(string & username, string & password);

    // links
    vector<Author> getAllBookAuthors(int book_id, int user_id);
    bool insertBookAuthor(int book_id, int author_id, int user_id);
    bool removeBookAuthor(int book_id, int author_id, int user_id);
    bool removeByBookId(int book_id, int user_id);//new
    bool removeByAuthorId(int author_id, int user_id);
    bool addUser(QString & username, QString & password);//new DONE

};
