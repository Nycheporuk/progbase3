#include "authorWindow.h"
#include "ui_authorWindow.h"

AuthorWindow::AuthorWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AuthorWindow)
{
    ui->setupUi(this);
}

AuthorWindow::~AuthorWindow()
{
    delete storage_;
    delete ui;
}


void AuthorWindow::load(QString file_name)
{
    qDebug() << "bookid: " << book_id_;
    qDebug() << "user_id: " << user_id_;

    file_name_ = file_name;
    SqliteStorage * sql_storage = new SqliteStorage(file_name_.toStdString());
    storage_ = sql_storage;

    if (!storage_->open())
    {
        qDebug() << "Can't open storage in authorWindow.cpp";
    }
    else
        qDebug() << "Opened storage in authorWindow.cpp";

    vector<Author> authors = storage_->getAllBookAuthors(book_id_, user_id_);

    for (size_t i = 0; i < authors.size(); i++)
    {
        cout << authors.at(i).name_ << endl;

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qBookListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(authors.at(i));

        QString name = QString::fromStdString(authors.at(i).name_);
        qBookListItem->setText(name);
        qBookListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qBookListItem);
    }

    ui->listWidget->setEnabled(true);
    ui->AddButton->setEnabled(true);
}

void AuthorWindow::on_AddButton_clicked()
{
    addDialog = new AddAuthorDialog(this);
    addDialog->show();
    connect(addDialog, SIGNAL(sendAuthor(Author*)), this, SLOT(getAuthor(Author*)));
}

void AuthorWindow::on_CloseButton_clicked()
{
    AuthorWindow::close();
}

void AuthorWindow::getUserId(int user_id)
{
    user_id_ = user_id;
}

void AuthorWindow::getBookId(int book_id)
{
    book_id_ = book_id;
}

void AuthorWindow::getAuthor(Author * author)
{
    int id = storage_->insertAuthor(*author, user_id_);
    if (id == 0)
    {
        qDebug() << "Can't insert in authorWindow.cpp";
        return;
    }
    qDebug() << "book_id: " << book_id_ << endl << "author_id: " << id << endl << "user_id: " << user_id_;
    if (!storage_->insertBookAuthor(book_id_, id, user_id_))
    {
        qDebug() << "Can't insert";
        return;
    }
    vector<Author> authors = storage_->getAllBookAuthors(book_id_, user_id_);

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qAuthorListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(authors.at(authors.size() - 1));

    QString name = QString::fromStdString(authors.at(authors.size() - 1).name_);
    qAuthorListItem->setText(name);
    qAuthorListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qAuthorListItem);
}

void AuthorWindow::getUpdAuthor(Author author)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Author a = var.value<Author>();
    author.id_ = a.id_;

    if(!storage_->updateAuthor(author))
    {
        qDebug() << "Can't update in getUpdAuthor";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }

    QListWidgetItem * qBookListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(author);

    QString name = QString::fromStdString(author.name_);
    qBookListItem->setText(name);
    qBookListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qBookListItem);
}

void AuthorWindow::on_RemoveButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
        qDebug() << "Yes was clicked";
    else
    {
        qDebug() << "Yes was *not* clicked";
        return;
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Author a = var.value<Author>();
        int ind = a.id_;
        qDebug() << "ind: " << ind;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeAuthor(ind, user_id_))
            qDebug() << "Can't remove author in authorswindow.cpp";
        else
        {
            if(!storage_->removeByAuthorId(ind, user_id_))
                qDebug() << "Can't remove connection in authorswindow.cpp";
            else
                qDebug() << "Removed author successfully in authorswindow.cpp";
        }
    }
    qDebug() << file_name_ << endl;

    vector<Author> author = storage_->getAllBookAuthors(book_id_, user_id_);
    if (author.empty())
    {
        ui->EditButton->setEnabled(false);
        ui->RemoveButton->setEnabled(false);

        ui->nameFillLabel->setText("-");
        ui->countryFillLabel->setText("-");
        ui->yearFillLabel->setText("-");
        return;
    }
    items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    if (item == nullptr)
    {
        return;
    }
    QVariant var = item->data(Qt::UserRole);
    Author auth = var.value<Author>();

    ui->nameFillLabel->setText(QString::fromStdString(auth.name_));
    ui->countryFillLabel->setText(QString::fromStdString(auth.country_));
    ui->yearFillLabel->setText(QString::fromStdString(to_string(auth.year_)));
}

void AuthorWindow::on_listWidget_itemClicked(QListWidgetItem * author)
{

    QVariant var = author->data(Qt::UserRole);
    Author auth = var.value<Author>();
    qDebug() << "auth.id_ in listWidget:" << auth.id_;

    ui->nameFillLabel->setText(QString::fromStdString(auth.name_));
    ui->countryFillLabel->setText(QString::fromStdString(auth.country_));
    ui->yearFillLabel->setText(QString::fromStdString(to_string(auth.year_)));
    ui->EditButton->setEnabled(true);
    ui->RemoveButton->setEnabled(true);

}

void AuthorWindow::on_EditButton_clicked()
{
    editDialog = new EditAuthorDialog(this);
    editDialog->show();

    connect(this, SIGNAL(sendAuthorToEditForm(QListWidgetItem*)), editDialog, SLOT(getAuthorToEdit(QListWidgetItem*)));
    connect(editDialog, SIGNAL(sendUpdatedAuthor(Author)), this, SLOT(getUpdAuthor(Author)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendAuthorToEditForm(item));

    ui->EditButton->setEnabled(false);
    ui->RemoveButton->setEnabled(false);
}

void AuthorWindow::getAuthorsToDelete(vector<int> author_id)
{
    qDebug() << "int ";
    for (size_t i = 0; i < author_id.size(); i++)
    {
        storage_->removeAuthor(author_id.at(i), user_id_);
    }
}
