#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "csv.hpp"
#include "user.h"

using std::string;
using std::vector;

class Storage
{

  public:
    virtual ~Storage() = default;

    virtual bool open() = 0;
    virtual bool close() = 0;


    // Books
    virtual vector<Book> getAllUserBooks(int user_id) = 0;
    virtual vector<Book> getAllBooks(void) = 0;
    virtual optional<Book> getBookById(int book_id) = 0;
    virtual bool updateBook(const Book & book) = 0;
    virtual bool removeBook(int book_id) = 0;
    virtual int insertBook(const Book & book) = 0;
    virtual int insertBook(const Book & book, int user_id) = 0;

    // authors



    virtual int insertAuthor(const Author & author, int user_id) = 0;
    virtual bool removeAuthor(int author_id, int user_id) = 0;
    virtual bool updateAuthor(const Author & author) = 0;
    virtual vector<Author> getAllUserAuthors(int user_id) = 0;

    // users
    virtual optional<User> getUserAuth(string & username, string & password) = 0;

    // links
    virtual vector<Author> getAllBookAuthors(int book_id, int user_id) = 0;
    virtual bool insertBookAuthor(int book_id, int author_id, int user_id) = 0;
    virtual bool removeBookAuthor(int book_id, int author_id, int user_id) = 0;
    virtual bool removeByBookId(int book_id, int user_id) = 0;
    virtual bool removeByAuthorId(int author_id, int user_id) = 0;
    virtual bool addUser(QString & username, QString & password) = 0;
    virtual vector<Book> getPageOfBooks(int user_id, int current_page, int page_size) = 0;

};
