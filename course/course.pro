#-------------------------------------------------
#
# Project created by QtCreator 2019-05-17T21:00:41
#
#-------------------------------------------------



QT       += core gui sql
CONFIG += c++14 console


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DEFINES += QT_DEPRECATED_WARNINGS



TARGET = course
TEMPLATE = app
INCLUDEPATH += "/home/nycheporuk/Programming/progbase3/course/StaticLib"
LIBS += "/home/nycheporuk/Programming/progbase3/course/build-StaticLib-Desktop-Debug/libStaticLib.a"

SOURCES += \
    main.cpp\
    mainWindow.cpp \
    addDialog.cpp \
    editDialog.cpp \
    addAuthorDialog.cpp \
    editAuthorDialog.cpp\
    authorWindow.cpp \
    authWindow.cpp \
    registrationwindow.cpp

HEADERS  += mainWindow.h \
    addDialog.h \
    editDialog.h\
    authorWindow.h \
    addAuthorDialog.h \
    editAuthorDialog.h\
    authWindow.h \
    registrationwindow.h \
    StaticLib/author.hpp \
    StaticLib/book.hpp \
    StaticLib/csv_storage.hpp \
    StaticLib/csv.hpp \
    StaticLib/generatorId.hpp \
    StaticLib/sqlite_storage.h \
    StaticLib/staticlib.h \
    StaticLib/storage.hpp \
    StaticLib/user.h


FORMS    += mainWindow.ui \
    addDialog.ui \
    editDialog.ui \
    addAuthorDialog.ui \
    editAuthorDialog.ui \
    authorWindow.ui \
    authWindow.ui \
    registrationwindow.ui

DISTFILES += \
    data/csv/books.csv\
    data/sql/data.sqlite

