#include "editAuthorDialog.h"
#include "ui_editAuthorDialog.h"

EditAuthorDialog::EditAuthorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditAuthorDialog)
{
    ui->setupUi(this);
}

EditAuthorDialog::~EditAuthorDialog()
{
    delete ui;
}
void EditAuthorDialog::on_Cansel_Ok_accepted()
{
    Author author;
    author.name_ = ui->nameLine->text().toStdString();
    author.country_ = ui->countryLine->text().toStdString();
    author.year_ = ui->yearSpinBox->value();

    emit sendUpdatedAuthor(author);
    EditAuthorDialog::close();
}


void EditAuthorDialog::on_Cansel_Ok_rejected()
{
    EditAuthorDialog::close();
}

void EditAuthorDialog::getAuthorToEdit(QListWidgetItem* item)
{
    QVariant variant = item->data(Qt::UserRole);
    Author author = variant.value<Author>();

    ui->nameLine->setText(QString::fromStdString(author.name_));
    ui->countryLine->setText(QString::fromStdString(author.country_));
    ui->yearSpinBox->setValue(author.year_);
}

